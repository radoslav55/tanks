
var timeLeft = 0;
var isDead = false;


var timerInterval = setInterval(function(){

    if(isDead){

        if(document.getElementById('respawnSeconds').innerHTML <= 1){

            document.getElementsByClassName('shadow-dead')[0].style.display = 'none';
            isDead = false;

        }
        document.getElementById('respawnSeconds').innerHTML = document.getElementById('respawnSeconds').innerHTML - 1;
    }

    if(timeLeft > 0){

        timeLeft -= 1;

        var secondsLeft = (timeLeft - Date.now()) / 1000;

        document.getElementById('gameTimer').innerHTML = (secondsLeft / 60 | 0) + ':' + (secondsLeft % 60 | 0);
    }

}, 1000);


function die(){

    document.getElementsByClassName('shadow-dead')[0].style.display = 'flex';

    document.getElementById('respawnSeconds').innerHTML = 7;

    isDead = true;

}


function handleGameActive(isGameActive, timeLeft){

    if(!isGameActive){

        document.getElementsByClassName('shadow-inactive')[0].style.display = 'flex';
    }
    else{

        beginGame(timeLeft);
    }
}

function beginGame(left){

    if(document.getElementsByClassName('shadow-inactive')[0]){
        document.getElementsByClassName('shadow-inactive')[0].style.display = 'none';
    }

    document.getElementsByClassName('shadow-game-end')[0].style.display = 'none';


    timeLeft = left;
}

function stopGame(data){

    timeLeft = -1;

    document.getElementsByClassName('shadow-game-end')[0].style.display = 'flex';

    data = JSON.parse(data);
    data = JSON.parse(data.data);

    var deadsTeamA = 0;
    var killsTeamA = 0;
    var deadsTeamB = 0;
    var killsTeamB = 0;

    var winner;

    for(var i in data){


        if(data[i].team == 1){

            deadsTeamA += parseInt(data[i].deads);
            killsTeamA += parseInt(data[i].kills);
        }
        else{

            deadsTeamB += parseInt(data[i].deads);
            killsTeamB += parseInt(data[i].kills);
        }
    }

    if(killsTeamA - deadsTeamA > killsTeamB - deadsTeamB){
        winner = 1;
    }
    else if(killsTeamA - deadsTeamA == killsTeamB - deadsTeamB){
        winner = 0;
    }
    else{
        winner = 2;
    }



    var html = '<h1>Game Over. Here is the result</h1><table>';

    html += '<tr><td width="100">Team</td><td width="100">Kills</td><td width="100">Deads</td></tr>';
    html += '<tr><td>Red Team</td><td>' + killsTeamA + '</td><td>' + deadsTeamA + '</td></tr>';
    html += '<tr><td>Blue Team</td><td>' + killsTeamB + '</td><td>' + deadsTeamB + '</td></tr>';

    if(winner == 0){
        html += '<tr><td colspan="3">Draw!</td> </tr>';
    }
    else{
        html += '<tr><td colspan="3">The winner is the ' + (winner == 1 ? 'Red Team' : 'Blue Team') + '</td> </tr>';
    }


    document.getElementById('end-dashboard').innerHTML = html;


}