function drawDashboard(input){

    var html = '<table style="text-align: left;">';

    html += '<thead style="text-align: left;"><th width="100">Name</th><th width="100">Kills</th><th width="100">Deaths</th><th width="100">Team</th></thead>';

    input = JSON.parse(input);



    for(var i in input){

        html += '<tr><td>' + input[i].name + '</td><td>' + input[i].kills + '</td><td>' + input[i].deads + '</td><td>' + (input[i].team == 1 ? 'Red Team' : 'Blue Team') + '</td></tr>';

    }

    html += '</table>';

    document.getElementById('gameTable').innerHTML = html;
}