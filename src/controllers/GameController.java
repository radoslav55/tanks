package controllers;


import app.Dashboard;
import app.Game;
import dao.BulletDAO;
import dao.TankDAO;
import models.Bullet;
import system.Calculate;
import system.exceptions.TankNotFoundException;
import lib.JSONException;
import lib.JSONObject;
import models.Map;
import models.Tank;
import system.ws.Endpoint;
import system.ws.Response;

import javax.websocket.Session;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameController {

    private Tank tank;

    public GameController(Session session) {

        try {

            tank = TankDAO.findTankBySession(session);

        } catch (TankNotFoundException e) {
            e.printStackTrace();
        }


    }

    /**
     * createUser method
     * <p>
     * Adds a new tank in the array and sends to the user an initial set of data, holding the map, his sessionID and more
     *
     * @param session
     */
    public static void createUser(Session session) {

        Tank tank = new Tank(session);
        TankDAO.add(tank);

        try {

            String jsonOutput = new JSONObject()
                    .put("type", "initial")
                    .put("map", Arrays.deepToString(Map.getMap()))
                    .put("sessionID", session.getId())
                    .toString();

            TankDAO.addToTeam(tank);

            Response.sendTo(session, jsonOutput);
            Response.sendTo(session, Dashboard.generate());

            jsonOutput = new JSONObject()
                    .put("type", "bullets")
                    .put("count", tank.getBullets())
                    .toString();

            Response.sendTo(session, jsonOutput);

        } catch (JSONException e) {

            System.out.println(e.getCause());
        }

    }

    /**
     * removeSession method
     * removes a tank from the Set
     *
     * @param session
     */
    public static void removeUser(Session session) {

        try {
            Tank tank = TankDAO.findTankBySession(session);
            TankDAO.remove(tank);
        } catch (TankNotFoundException e) {

        }
    }


    public void movingUp() {

        tank.setMovingWith(1);

    }

    public void movingDown() {

        tank.setMovingWith(-1);
    }

    public void rotatingLeft() {

        tank.setRotatingWith(-1);
    }

    public void rotatingRight() {

        tank.setRotatingWith(1);

    }

    public void stopMoving() {

        tank.setMovingWith(0);
    }

    public void stopRotating() {

        tank.setRotatingWith(0);

    }

    public void login(String name) {

        tank.setName(name);
        tank.setIsPlaying(true);
        tank.setPrimarySet(true);
        TankDAO.respawn(tank);

        String msg = new JSONObject()
                .put("type", "login")
                .put("isGameActive", Game.isGameActive())
                .put("leftTime", Game.getGameEnd())
                .toString();

        Response.sendTo(tank.getSession(), msg);

        msg = new JSONObject()
                .put("type", "bullets")
                .put("count", 35)
                .toString();

        Response.sendToAll(msg);

        Dashboard.update();
    }

    public void startGame() {

        Game.setIsGameActive(true);
        Game.setGameEnd(System.currentTimeMillis() + (1000 * 60 * 3));

        TankDAO.nullALL();

        String msg = new JSONObject()
                .put("type", "gameStart")
                .put("leftTime", Game.getGameEnd())
                .toString();

        Response.sendToAll(msg);
    }

    public void shoot() {

        if (tank.getBullets() > 0) {
            String msg = new JSONObject()
                    .put("type", "shoot")
                    .put("sessionID", tank.getSession())
                    .put("x", tank.getFigure().getX())
                    .put("y", tank.getFigure().getY())
                    .toString();

            Response.sendToAll(msg);

            double[] coords = Calculate.getRealCoords(tank.getFigure().getX(), tank.getFigure().getY(), tank.getFigure().getWidth() / 2, 0, tank.getFigure().getRotation(), tank.getFigure().getWidth(), tank.getFigure().getHeight());


            tank.setBullets(tank.getBullets() - 1);

            msg = new JSONObject()
                    .put("type", "bullets")
                    .put("count", tank.getBullets())
                    .toString();

            Response.sendTo(tank.getSession(), msg);

            Bullet bullet = new Bullet();
            bullet.setX(coords[0]);
            bullet.setY(coords[1]);
            bullet.setOrigin(tank.getSession());
            bullet.setRotation(tank.getFigure().getRotation());

            BulletDAO.add(bullet);
        }
    }

    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }
}
