package app;

/**
 * Created by radoslavstefanov on 6/8/17.
 */
public class Game {

    private static boolean isGameActive = false;
    private static long gameEnd;

    public static boolean isGameActive() {
        return isGameActive;
    }

    public static void setIsGameActive(boolean isGameActive) {
        Game.isGameActive = isGameActive;
    }

    public static long getGameEnd() {
        return gameEnd;
    }

    public static void setGameEnd(long gameEnd) {
        Game.gameEnd = gameEnd;
    }
}
