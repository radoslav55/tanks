package app;

import dao.TankDAO;
import lib.JSONObject;
import models.Map;
import models.Tank;
import system.ws.Endpoint;
import system.ws.Response;

import java.util.Arrays;

/**
 * Created by radoslavstefanov on 6/7/17.
 */
public class Dashboard {


    public static String generate(){



        String json = "[";

        for(Tank tank : TankDAO.all()){

            if(tank.isPrimarySet()){
                json += tank.exportJsonDashboard() + ",";
            }



        }

        if(json.length() > 1){
            json = json.substring(0, json.length()-1);
        }

        json += "]";


         return new JSONObject()
                .put("type", "dashboard")
                .put("data", json)
                .toString();

    }


    public static void update(){

        Response.sendToAll(Dashboard.generate());
    }
}
