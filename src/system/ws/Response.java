package system.ws;

import dao.TankDAO;
import models.Tank;
import system.exceptions.TankNotFoundException;

import javax.websocket.Session;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by radoslavstefanov on 4/6/17.
 */
public class Response {

    public static void sendTo(Session session, String message){

        try {

            session.getBasicRemote().sendText(message);

        } catch (IOException ex) {

            try{

                TankDAO.remove(TankDAO.findTankBySession(session));
                Logger.getLogger(Endpoint.class.getName()).log(Level.SEVERE, null, ex);
            }
            catch(TankNotFoundException e){

                System.out.println("The tank was not found");
            }

        }

    }

    public static void sendToAll(String message){

        for(Tank tank : TankDAO.all()){

            Response.sendTo(tank.getSession(), message);
        }
    }
}
