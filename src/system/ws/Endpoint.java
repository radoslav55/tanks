package system.ws;

import app.Dashboard;
import app.Game;
import controllers.GameController;
import system.Loop;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Created by radoslavstefanov on 4/5/17.
 */

@ServerEndpoint("/game")
public class Endpoint {

    public Endpoint() {

        Loop.startIfNotStarted();
    }


    @OnOpen
    public void onOpen(Session session) {

        GameController.createUser(session);
    }

    @OnMessage
    public void echo(String message, Session session) {

        GameController controller = new GameController(session);

        try {

            String[] parts = message.split(":");


            if((Game.isGameActive() && controller.getTank().isPlaying()) || parts[0].equals("startGame") || parts[0].equals("login")){

                Method method;

                if (parts.length > 1) {

                    method = controller.getClass().getMethod(parts[0], parts[1].getClass());
                    method.invoke(controller, parts[1]);
                } else {

                    method = controller.getClass().getMethod(parts[0]);
                    method.invoke(controller);
                }
            }

        } catch (NoSuchMethodException e) {
            System.out.println("NO method");
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @OnError
    public void onError(Throwable t) {


    }

    @OnClose
    public void onClose(Session session) {

        GameController.removeUser(session);
        Dashboard.update();
    }
}
