package system;

/**
 * Created by radoslavstefanov on 4/30/17.
 */
public class Square {


    //   A         B
    //   -----------
    //   |         |
    //   |         |
    //   |         |
    //   |         |
    //   -----------
    //   C         D


    private Point pointA;
    private Point pointB;
    private Point pointC;
    private Point pointD;

    private double rotation = 0;

    private int width;
    private int height;

    public Square(Point pointA, int width, int height) {

        this.pointA = pointA;
        this.pointB = new Point(pointA.getX() + width, pointA.getY());
        this.pointC = new Point(pointA.getX(), pointA.getY());
        this.pointD = new Point(pointA.getX() + width, pointA.getY());

        this.width = width;
        this.height = height;

    }

    public Square(Point pointA, Point pointB, Point pointC, Point pointD) {

        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;
        this.pointD = pointD;

        this.width = (int) (pointA.getX() - pointB.getX());
        this.height = (int) (pointA.getY() - pointC.getY());
    }

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }


    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public Point getPointD() {
        return pointD;
    }


    public void setPointD(Point pointD) {
        this.pointD = pointD;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public double getWidth() {

        return this.width;
    }

    public double getHeight() {

        return this.height;
    }

    public double getX() {

        return pointA.getX();
    }

    public double getY() {

        return pointA.getY();
    }

    public void setX(double x) {

        pointA.setX(x);
        pointB.setX(x + width);
        pointC.setX(x);
        pointD.setX(x + width);

    }

    public void setY(double y) {

        pointA.setY(y);
        pointB.setY(y);
        pointC.setY(y + height);
        pointD.setY(y + height);

    }


    public Point getAbsolutepointA() {

        return pointA.getRealCoords(0, 0, rotation, getWidth(), getHeight());
    }

    public Point getAbsolutepointB() {

        return pointA.getRealCoords(getWidth(), 0, rotation, getWidth(), getHeight());
    }

    public Point getAbsolutepointC() {

        return pointA.getRealCoords(0, getHeight(), rotation, getWidth(), getHeight());
    }

    public Point getAbsolutepointD() {

        return pointA.getRealCoords(getWidth(), getHeight(), rotation, getWidth(), getHeight());
    }

    public boolean isPointInside(Point point, int area) {

        double triangleABPArea = Math.abs((getAbsolutepointA().getX() * (getAbsolutepointB().getY() - point.getY()) + getAbsolutepointB().getX() * (point.getY() - getAbsolutepointA().getY()) + point.getX() * (getAbsolutepointA().getY() - getAbsolutepointB().getY())) / 2);
        double triangleACPArea = Math.abs((getAbsolutepointA().getX() * (getAbsolutepointC().getY() - point.getY()) + getAbsolutepointC().getX() * (point.getY() - getAbsolutepointA().getY()) + point.getX() * (getAbsolutepointA().getY() - getAbsolutepointC().getY())) / 2);
        double triangleCDPArea = Math.abs((getAbsolutepointC().getX() * (getAbsolutepointD().getY() - point.getY()) + getAbsolutepointD().getX() * (point.getY() - getAbsolutepointC().getY()) + point.getX() * (getAbsolutepointC().getY() - getAbsolutepointD().getY())) / 2);
        double triangleDBPArea = Math.abs((getAbsolutepointD().getX() * (getAbsolutepointB().getY() - point.getY()) + getAbsolutepointB().getX() * (point.getY() - getAbsolutepointD().getY()) + point.getX() * (getAbsolutepointD().getY() - getAbsolutepointB().getY())) / 2);


        if ((triangleABPArea + triangleACPArea + triangleCDPArea + triangleDBPArea) <= area) {

            return true;

        }

        return false;
    }

    public boolean isSquareInside(Square square, int area) {

        return ( this.isPointInside(square.getAbsolutepointA(), area) || this.isPointInside(square.getAbsolutepointB(), area) || this.isPointInside(square.getAbsolutepointC(), area) || this.isPointInside(square.getAbsolutepointD(), area) );
    }


}
