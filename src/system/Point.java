package system;

/**
 * Created by radoslavstefanov on 4/30/17.
 */
public class Point {

    private double x;
    private double y;

    public Point(double x, double y){

        this.x = x;
        this.y = y;
    }

    public static Point make(double x, double y){

        return new Point(x, y);
    }

    public void setX(double x) {

        this.x = x;
    }

    public double getX(){

        return x;
    }

    public double getY() {

        return y;
    }

    public void setY(double y) {

        this.y = y;
    }

    public Point getRealCoords(double xAlter, double yAlter, double rotation, double width, double height) {

        double centerX = x + (width / 2);
        double centerY = y + (height / 2);

        return new Point(
                (Math.cos(-rotation) * (x + xAlter - centerX)) - (Math.sin(-rotation) * (y + yAlter - centerY)) + centerX,
                (Math.sin(rotation) * (x + xAlter - centerX)) - (Math.cos(rotation) * (y + yAlter - centerY)) + centerY
        );

    }

    public Point add(double x, double y){

        this.x += x;
        this.y += y;

        return this;

    }
}
