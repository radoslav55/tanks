package system;

import models.Map;

/**
 * Created by radoslavstefanov on 4/6/17.
 */
public class Calculate {

    public static double[] getRealCoords(double x, double y, double xAlter, double yAlter, double rotation, double width, double height) {

        double[] output = new double[2];

        double centerX = x + (width / 2);
        double centerY = y + (height / 2);

        output[0] = (Math.cos(rotation) * (x + xAlter - centerX)) - (Math.sin(rotation) * (y + yAlter - centerY)) + centerX;
        output[1] = (Math.sin(rotation) * (x + xAlter - centerX)) + (Math.cos(rotation) * (y + yAlter - centerY)) + centerY;

        return output;
    }


    public static boolean isObjectInMap(double x, double y, double width, double height, double rotation) {

        return  Map.canBeInside(Calculate.getRealCoords(x, y, 0, 0, rotation, width, height)) &&
                Map.canBeInside(Calculate.getRealCoords(x, y, width, 0, rotation, width, height)) &&
                Map.canBeInside(Calculate.getRealCoords(x, y, 0, height, rotation, width, height)) &&
                Map.canBeInside(Calculate.getRealCoords(x, y, width, height, rotation, width, height));

    }
}
