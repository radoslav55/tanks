/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package system.exceptions;

/**
 *
 * @author Radoslav
 */
public class TankNotFoundException extends Exception {

    /**
     * Creates a new instance of <code>TankNotFoundException</code> without
     * detail message.
     */
    public TankNotFoundException() {
    }

    /**
     * Constructs an instance of <code>TankNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public TankNotFoundException(String msg) {
        super(msg);
    }
}
