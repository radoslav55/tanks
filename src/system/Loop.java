package system;

import app.Dashboard;
import app.Game;
import controllers.GameController;
import dao.BulletDAO;
import dao.ItemDAO;
import dao.TankDAO;
import lib.JSONObject;
import models.Bullet;
import models.Item;
import models.Map;
import models.Tank;
import system.exceptions.TankNotFoundException;
import system.ws.Response;

import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by radoslavstefanov on 4/5/17.
 */
public class Loop {

    private static boolean loopStarted = false;


    public static void startIfNotStarted(){


        if(!loopStarted){

            loopStarted = true;

            ExecutorService service = Executors.newFixedThreadPool(1);

            service.submit(new Runnable() {
                public void run() {
                    gameLoop();
                }
            });
        }

    }

    private static void gameLoop(){

        try{
            updateGame();
            displayGame();
            Thread.sleep(40);
            gameLoop();
        }
        catch(Exception e){

            System.out.println(e.getMessage());
        }
    }

    private static void displayGame(){

        String tanksJsonData = "[";


        if(Game.isGameActive()){

            for(Tank tank : TankDAO.all()){

                if(tank.isPrimarySet()){

                    tanksJsonData += tank.exportJson() + ",";
                }

            }
        }


        if(tanksJsonData.length() > 1){
            tanksJsonData = tanksJsonData.substring(0, tanksJsonData.length()-1);
        }


        tanksJsonData += "]";


        String bulletsJsonData = "[";

        for(Bullet bullet : BulletDAO.all()){

            bulletsJsonData += bullet.exportJson() + ",";
        }

        if(!BulletDAO.all().isEmpty())
            bulletsJsonData = bulletsJsonData.substring(0, bulletsJsonData.length()-1);

        bulletsJsonData += "]";

        String itemsJsonData = "[";

        for(Item item : ItemDAO.all()){

            itemsJsonData += item.exportJson() + ",";
        }

        if(!ItemDAO.all().isEmpty())
            itemsJsonData = itemsJsonData.substring(0, itemsJsonData.length()-1);

        itemsJsonData += "]";


        try{
            String provider = new JSONObject()
                    .put("type", "loop")
                    .put("tanks", tanksJsonData)
                    .put("bullets", bulletsJsonData)
                    .put("items", itemsJsonData)
                    .toString();

            for(Tank tank : TankDAO.all()){

                Response.sendTo(tank.getSession(), provider);
            }
        }
        catch(Exception e){

            System.out.println("Exceptioooon");
        }



    }

    private static void updateGame(){

        HashSet<Bullet> bulletsToRemove = new HashSet<>();
        HashSet<Item> itemsToRemove = new HashSet<>();

        if(Game.isGameActive() && Game.getGameEnd() < System.currentTimeMillis()){

            Game.setIsGameActive(false);

            String provider = new JSONObject()
                    .put("type", "gameEnd")
                    .put("game", Dashboard.generate())
                    .toString();

            Response.sendToAll(provider);
        }

        for(Tank tank : TankDAO.all()){

            if(tank.isPlaying()){

                tank = TankDAO.move(tank);

                for(Bullet bullet : BulletDAO.all()) {

                    if (bullet.isHitting(tank.getFigure())) {

                        if(tank.getSession() != bullet.getOrigin()){

                            tank.setHealth(tank.getHealth() - 10);
                            if(TankDAO.checkIfDead(tank)){

                                try{
                                    TankDAO.findTankBySession(bullet.getOrigin()).increaseKills();
                                    Dashboard.update();
                                }
                                catch(TankNotFoundException e){

                                    System.out.println("Tank not found!");
                                }
                            }

                            bulletsToRemove.add(bullet);

                        }


                    }
                }

                for(Item item : ItemDAO.all()){

                    if(item.getFigure().isSquareInside(tank.getFigure(), 910)){

                        itemsToRemove.add(item);

                        if(item.getType() == "health"){

                            tank.addHealth(item.getCount());
                        }
                        else{

                            tank.addBullets(item.getCount());

                            String msg = new JSONObject()
                                    .put("type", "bullets")
                                    .put("count", tank.getBullets())
                                    .toString();

                            Response.sendTo(tank.getSession(), msg);
                        }
                    }
                }
            }
            else{

                if(tank.isPrimarySet() && tank.getRespawnOn() < System.currentTimeMillis()){

                    tank.setIsPlaying(true);
                    TankDAO.respawn(tank);

                }
            }

        }

        Random rand = new Random();

        if(rand.nextInt(900) == 3){

            Item item = new Item();
            item.setFigure(new Square(Map.getRandomActivePoint(), 30, 30));
            item.setRandomType();

            ItemDAO.add(item);
        }

        for(Bullet bullet : BulletDAO.all()){

            if(!bullet.isCrashed()){
                bulletsToRemove.add(bullet);
            }

            bullet.update();

        }

        BulletDAO.removeGroup(bulletsToRemove);
        ItemDAO.removeGroup(itemsToRemove);


    }
}
