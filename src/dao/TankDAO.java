package dao;

import lib.JSONObject;
import models.Bullet;
import models.Item;
import models.Map;
import models.Tank;
import system.Calculate;
import system.Point;
import system.Square;
import system.exceptions.TankNotFoundException;
import system.ws.Response;

import javax.websocket.Session;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by martintsvetanov on 30/02/2017 <3<3
 */
public class TankDAO {

    private static Set<Tank> tanks = new HashSet<>();

    public static Set<Tank> all() {

        return tanks;
    }

    public static void setTanks(Set<Tank> tanks) {

        TankDAO.tanks = tanks;
    }

    public static void add(Tank tank) {

        tanks.add(tank);
    }

    public static void remove(Tank tank) {

        tanks.remove(tank);
    }

    public static Tank findTankBySession(Session session) throws TankNotFoundException {

        for (Tank tank : tanks) {
            if (tank.getSession() == session) {
                return tank;
            }
        }

        throw new TankNotFoundException();
    }

    public static void update(Session session, Tank newTank) {

        for (Tank tank : tanks) {

            if (tank.getSession() == newTank.getSession()) {
                tank = newTank;
            }
        }
    }

    public static boolean checkIfDead(Tank tank) {

        if (tank.getHealth() <= 0) {

            tank.setHealth(100);
            tank.setIsPlaying(false);
            tank.getFigure().setRotation(0);
            tank.setDeads(tank.getDeads() + 1);

            tank.setRespawnOn(System.currentTimeMillis() + (7 * 1000));

            Response.sendTo(tank.getSession(), new JSONObject()
                    .put("type", "dead").toString());

            Response.sendToAll(new JSONObject()
                    .put("type", "explosion")
                    .put("x", tank.getFigure().getPointA().getX())
                    .put("y", tank.getFigure().getPointA().getY())
                    .toString()
            );

            Item item = new Item();
            item.setFigure(new Square(tank.getFigure().getPointA(), 30, 30));
            item.setRandomType();

            ItemDAO.add(item);

            return true;
        }

        return false;
    }

    public static void respawn(Tank tank) {

        Point newLocation = Map.getRandomActivePoint();

        tank.getFigure().setPointA(newLocation);


    }


    public static void addToTeam(Tank tank){

        int team1 = 0;
        int team2 = 0;

        for (Tank currTank : tanks) {

            if (currTank.getTeam() == 1) {
                team1++;
            }
            else{
                team2++;
            }
        }

        if(team1 < team2){
            tank.setTeam(1);
        }
        else{
            tank.setTeam(2);
        }
    }

    public static void nullALL(){

        for (Tank tank : tanks) {
            tank.setKills(0);
            tank.setDeads(0);
            tank.setBullets(35);
        }

    }

    public static Tank move(Tank tank) {

        double newX = tank.getFigure().getX(), newY = tank.getFigure().getY(), newRotation = tank.getFigure().getRotation();

        if (tank.getMovingWith() != 0) {


            newX += Math.sin(tank.getFigure().getRotation()) * (tank.getMovingWith() * tank.getSpeed());
            newY -= Math.cos(tank.getFigure().getRotation()) * (tank.getMovingWith() * tank.getSpeed());


        }
        if (tank.getRotatingWith() != 0) {

            newRotation = Math.toRadians(Math.toDegrees(newRotation) + (tank.getRotatingWith() * tank.getHandling()));
        }

        if (tank.getMovingWith() != 0 || tank.getRotatingWith() != 0) {

            TankDAO.moveToIfPossible(tank, newX, newY, newRotation);
        }

        return tank;
    }

    private static void moveToIfPossible(Tank tank, double x, double y, double rotation) {

        if (Calculate.isObjectInMap(x, y, tank.getFigure().getWidth(), tank.getFigure().getHeight(), rotation)) {

            tank.getFigure().setX(x);
            tank.getFigure().setY(y);
            tank.getFigure().setRotation(rotation);
        }
    }

}
