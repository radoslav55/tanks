package dao;

import models.Bullet;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by radoslavstefanov on 4/6/17.
 */
public class BulletDAO {


    private static Set<Bullet> bullets = new HashSet<>();


    public static void add(Bullet bullet){

        bullets.add(bullet);
    }

    public static void remove(Bullet bullet){

        bullets.remove(bullet);
    }

    public static Set<Bullet> all() {
        return bullets;
    }

    public static void setBullets(Set<Bullet> bullets) {

        BulletDAO.bullets = bullets;
    }

    public static void removeGroup(HashSet<Bullet> bulletsToRemove){

        for(Bullet bullet : bulletsToRemove){

            BulletDAO.remove(bullet);
        }
    }
}
