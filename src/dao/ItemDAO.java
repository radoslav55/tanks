package dao;

import models.Bullet;
import models.Item;
import models.Tank;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by radoslavstefanov on 6/14/17.
 */
public class ItemDAO {


    private static Set<Item> items = new HashSet<>();

    public static Set<Item> all() {

        return items;
    }

    public static void setItems(Set<Item> items) {

        ItemDAO.items = items;
    }

    public static void add(Item item){

        items.add(item);
    }

    public static void remove(Item item){

        items.remove(item);
    }

    public static void removeGroup(HashSet<Item> itemsToRremove){

        for(Item item : itemsToRremove){

            ItemDAO.remove(item);
        }
    }
}
