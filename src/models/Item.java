package models;

import lib.JSONObject;
import system.Square;

import java.util.Random;

/**
 * Created by radoslavstefanov on 6/14/17.
 */
public class Item {


    private Square figure;
    private String type;
    private int count;

    public Square getFigure() {
        return figure;
    }

    public void setFigure(Square figure) {
        this.figure = figure;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setRandomType(){

        Random rand = new Random();
        int type = rand.nextInt(2);

        if(type == 0){
            this.type = "health";
            this.count = rand.nextInt(20) + 20;
        }
        if(type == 1){
            this.type = "bullets";
            this.count = rand.nextInt(10) + 15;
        }
        if(type == 2){
            this.type = "mines";
            this.count = rand.nextInt(2) + 1;
        }

    }

    public String exportJson(){

        return new JSONObject()
                .put("x", this.figure.getPointA().getX())
                .put("y", this.figure.getPointA().getY())
                .put("type", this.type)
                .toString();
    }
}
