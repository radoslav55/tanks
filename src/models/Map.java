package models;

import system.Point;

import java.util.Random;

/**
 * @author Radoslav
 */
public class Map {

    private final static int squareWidth = 130;
    private final static int squareHeight = 130;

    //    private static final int[][] map = {
//            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//            {0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0},
//            {0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,1,0},
//            {0,1,1,1,0,0,0,1,1,0,0,0,0,1,1,1,1,1,1,1,0},
//            {0,1,1,1,0,0,0,1,1,0,0,0,1,1,1,1,1,0,1,1,0},
//            {0,1,1,1,0,0,0,1,1,1,1,1,1,1,0,0,0,0,1,1,0},
//            {0,1,1,1,1,1,0,0,1,1,1,1,1,1,0,0,0,0,1,1,0},
//            {0,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,1,1,0},
//            {0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,1,1,1,1,0},
//            {0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,1,1,1,0},
//            {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
//    };
    private static final int[][] map = {
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0},
            {0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
            {0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0},
            {0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 0},
            {0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0},
            {0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0},
            {0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0},
            {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
    };

    public static Point getRandomActivePoint() {

        int pointCode = 0;
        int x = 0;
        int y = 0;

        Random rand = new Random();

        while (pointCode != 1) {

            y = rand.nextInt(map.length);
            x = rand.nextInt(map[0].length);

            pointCode = Map.map[y][x];
        }

        System.out.println(x);
        System.out.println(y);

        return new Point(x * 130 + 30, y * 130 + 20);
    }

    public static int[][] getMap() {

        return map;
    }

    public static boolean canBeInside(double[] coords) {

        int arrX = (int) coords[0] / squareWidth;
        int arrY = (int) coords[1] / squareHeight;

        return map[arrY][arrX] == 1;
    }


}
