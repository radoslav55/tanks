package models;

import dao.TankDAO;
import lib.JSONObject;
import system.Point;
import system.Square;

import java.math.BigDecimal;
import java.util.Arrays;
import javax.websocket.Session;

/**
 * @author Radoslav
 */
public class Tank {

    private final Session session;
    private String name;
    private Square figure;
    private int health;
    private int kills;
    private int deads;
    private double movingWith;
    private double rotatingWith;
    private double speed;
    private double handling;

    private int team;
    private int bullets = 35;

    private boolean isPlaying = false;
    private boolean isPrimarySet = false;


    private long respawnOn = 0;


    public Tank(Session session) {
        this.session = session;

        this.figure = new Square(Point.make(150, 150), 50, 80);
        this.speed = 6;
        this.handling = 3;
        this.health = 100;
    }

    public int getTeam() {
        return team;
    }

    public void setTeam(int team) {
        this.team = team;
    }

    public int getBullets() {
        return bullets;
    }

    public void setBullets(int bullets) {
        this.bullets = bullets;
    }

    public void addBullets(int bullets){

        this.bullets += bullets;
    }

    public Session getSession() {
        return session;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getKills() {
        return kills;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public void increaseKills(){

        this.setKills(this.getKills() + 1);
    }

    public int getDeads() {
        return deads;
    }

    public void setDeads(int deads) {
        this.deads = deads;
    }

    public double getMovingWith() {
        return movingWith;
    }

    public void setMovingWith(double movingWith) {
        this.movingWith = movingWith;
    }

    public double getRotatingWith() {
        return rotatingWith;
    }

    public void setRotatingWith(double rotatingWith) {
        this.rotatingWith = rotatingWith;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getHandling() {
        return handling;
    }

    public void setHandling(double handling) {
        this.handling = handling;
    }

    public Square getFigure() {
        return figure;
    }

    public void setFigure(Square figure) {
        this.figure = figure;
    }

    public int getHealth() {

        return this.health;
    }

    public void setHealth(int health) {

        this.health = health;
    }

    public void addHealth(int health){

        this.health += health;

        if(this.health > 100) this.health = 100;
    }

    public boolean isPlaying() {

        return this.isPlaying;
    }

    public void setIsPlaying(boolean isPlaying) {

        this.isPlaying = isPlaying;
    }

    public void setRespawnOn(long respawnOn){

        this.respawnOn = respawnOn;
    }

    public long getRespawnOn(){

        return this.respawnOn;
    }

    public boolean isPrimarySet() {
        return isPrimarySet;
    }

    public void setPrimarySet(boolean primarySet) {
        isPrimarySet = primarySet;
    }

    public String exportJson() {

        String json = new JSONObject()
                .put("sessionID", session.getId())
                .put("name", name)
                .put("x", figure.getPointA().getX())
                .put("y", figure.getPointA().getY())
                .put("rotation", figure.getRotation())
                .put("team", team)
                .put("health", this.getHealth())
                .put("isPlaying", this.isPlaying)
                .toString();


        return json;
    }

    public String exportJsonDashboard(){

        String json = new JSONObject()
                .put("sessionID", session.getId())
                .put("name", name)
                .put("kills", kills)
                .put("deads", deads)
                .put("team", team)
                .toString();


        return json;
    }

}
