package models;

import lib.JSONObject;
import system.Calculate;
import system.Point;
import system.Square;

import javax.websocket.Session;

/**
 * @author Radoslav
 */
public class Bullet {

    private Session origin;
    private double x;
    private double y;
    private double rotation;

    private double speed = 25;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getRotation() {
        return rotation;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public void setOrigin(Session session){

        this.origin = session;
    }

    public Session getOrigin(){

        return this.origin;
    }

    public void update() {

        x += Math.sin(this.getRotation()) * this.speed;
        y -= Math.cos(this.getRotation()) * this.speed;


    }

    public boolean isCrashed() {

        return Calculate.isObjectInMap(x, y, 5, 5, this.getRotation());
    }

    public boolean isHitting(Square object) {

        return object.isPointInside(Point.make(x, y), 4010);
    }

    public String exportJson() {

        String json = new JSONObject()
                .put("x", x)
                .put("y", y)
                .put("rotation", rotation)
                .toString();

        return json;
    }
}
