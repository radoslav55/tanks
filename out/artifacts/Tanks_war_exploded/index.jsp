<!DOCTYPE html>
<html>
<head>
    <title>Tanks</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/app.css"/>
    <link rel="stylesheet" href="css/dashboard.css"/>
</head>
<body>
<canvas id="game" width="600" height="300">
    Your browser doesn't support canvas :(
</canvas>
<div class="fps-box"><span id="fps"></span> fps</div>

<div class="shadow shadow-login">

    <div id="login-box">
        <label for="usernameLoginField">Type your username</label>

        <input type="text" id="usernameLoginField">
        <button id="loginButton">Play!</button>
    </div>
</div>

<div class="shadow shadow-dead">
    <h1>
        You are dead! Respawn in <span id="respawnSeconds">10</span> seconds
    </h1>
</div>

<div class="shadow shadow-inactive">
    <h1>
        Waiting for the start of the game
    </h1>
</div>

<div class="shadow shadow-game-end">

    <div id="end-dashboard">

    </div>
</div>

<div id="dashboard">

    <div>
        <div style="margin: 5px 0; ">
            Time Left: <span id="gameTimer"></span><br>
            Bullets: <span id="bullets"></span><br>
        </div>

        <div id="gameTable">

        </div>

    </div>
</div>


<script src="js/app.js"></script>
<script src="js/keys.js"></script>
<script src="js/controls.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/actions.js"></script>
</body>
</html>
