var socket = new WebSocket("ws://192.168.8.100:8080/Tanks_war_exploded/game");

var c = document.getElementById('game');
c.width = window.innerWidth;
c.height = window.innerHeight;

var ctx = c.getContext('2d');

var map = [];
var sessionID;
var fps = 0;

var me = {};

var explosion = {
    step: -1,
    x: 0,
    y: 0,
}


var settings = {

    tank: {
        width: 50,
        height: 80
    },
    textures: {
        wall: (function() {
            var img = document.createElement('img');
            img.setAttribute('src', 'img/wall.jpeg');

            return img;
        }()),
        tank: (function(){

            var img = document.createElement('img');
            img.setAttribute('src', 'img/tank.png');

            return img;
        }()),
        ground: (function(){

            var img = document.createElement('img');
            img.setAttribute('src', 'img/asphalt.jpg');

            return img;
        }())
    },
    items:{
        health: (function(){

            var img = document.createElement('img');
            img.setAttribute('src', 'img/health.png');

            return img;
        }()),
        bullets: (function(){

            var img = document.createElement('img');
            img.setAttribute('src', 'img/ammo.png');

            return img;
        }()),
        mines: (function(){

            var img = document.createElement('img');
            img.setAttribute('src', 'img/mines.png');

            return img;
        }())
    },
    audio: {
        shoot: 'audio/boom.wav'

    }
};

setInterval(function(){
    document.getElementById('fps').innerHTML = fps;
    fps = 0;

}, 1000);

socket.onopen = function(){

    console.log('Successfully connected to the server');
};

var loop = true;

socket.onmessage = function(message){

    fps++;

    if(loop){

        try{
            message = JSON.parse(message.data);
        }
        catch(e){

            console.log(message.data);
            console.log('Error');
        }
        
        var tanks = [];
        var bullets = [];
        var items = [];

        if(message.type == 'initial'){

            map = JSON.parse(message.map);
            sessionID = message.sessionID;

        }
        else if(message.type == 'dashboard'){

            drawDashboard(message.data);
        }
        else if(message.type == "shoot"){

            new Audio(settings.audio.shoot).play();
        }
        else if(message.type == 'dead'){

            die();
        }
        else if(message.type == 'login'){

            handleGameActive(message.isGameActive, message.leftTime);
        }
        else if(message.type == 'gameStart'){

            beginGame(message.leftTime);
        }
        else if(message.type == 'gameEnd'){

            stopGame(message.game);
        }
        else if(message.type == 'explosion'){

            explosion.step = 0;
            explosion.x = message.x;
            explosion.y = message.y;
        }
        else if(message.type == "bullets"){

            document.getElementById('bullets').innerHTML = message.count;
        }
        else if(message.type == 'loop'){

            //console.log(message.tanks);

            tanks = JSON.parse(message.tanks);
            bullets = JSON.parse(message.bullets);
            items = JSON.parse(message.items);


            var centerX, centerY;

            ctx.setTransform(1,0,0,1,0,0);
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);


            var camX, camY;

            for(var i in tanks){

                if(tanks[i].sessionID == sessionID){

                    me.x = tanks[i].x;
                    me.y = tanks[i].y;

                    //Clamp the camera position to the world bounds while centering the camera around the player
                    camX = clamp(-tanks[i].x + ctx.canvas.width/2, -ctx.canvas.width, 0);
                    camY = clamp(-tanks[i].y + ctx.canvas.height/2, -ctx.canvas.height, 0);

                    ctx.translate( camX, camY );
                }
            }

            ctx.beginPath();
            ctx.drawImage(settings.textures.ground, 0, 0, map[0].length * 130, map.length * 130);
            ctx.fill();


            for(var i in bullets) {

                ctx.beginPath();
                ctx.fillStyle = '#333333';
                ctx.rect(bullets[i].x, bullets[i].y, 5, 5);
                ctx.fill();
            }



            for(var i in map){
                for(var j in map[i]){



                    if(map[i][j] == 0){
                        //ctx.fillStyle = "#471C01";
                        //ctx.rect(j * 100, i * 100, 100, 100);
                        ctx.beginPath()
                        ctx.drawImage(settings.textures.wall, j * 130, i * 130, 130, 130);
                        ctx.fill();
                    }


                }
            }


            if(explosion.step >= 0){

                console.log('here');

                ctx.drawImage(getExplosion(), 0, 0, 500, 500, explosion.x, explosion.y, 150, 150);
            }

            for(var i in items){

                ctx.drawImage(settings.items[items[i].type], items[i].x, items[i].y, 30, 30);

            }

            for(var i in tanks){




                if(tanks[i].isPlaying){

                    //ctx.setTransform(1,0,0,1,0,0);

                    centerX = tanks[i].x + (50 / 2);
                    centerY = tanks[i].y + (80 / 2);

                    ctx.beginPath();

                    ctx.fillStyle = '#000000';
                    ctx.translate(centerX, centerY);
                    ctx.rotate(tanks[i].rotation);
                    ctx.translate(-centerX, -centerY);

                    ctx.drawImage(settings.textures.tank, tanks[i].x, tanks[i].y, settings.tank.width, settings.tank.height);

                    ctx.setTransform(1,0,0,1,0,0);
                    ctx.translate( camX, camY );

                    ctx.font = "13px Arial";
                    ctx.textAlign="center";
                    ctx.fillStyle = tanks[i].team == 1 ? 'red' : 'blue';
                    ctx.fillText(tanks[i].name,tanks[i].x + settings.tank.width / 2, tanks[i].y - 30);

                    ctx.beginPath()
                    ctx.lineWidth = 1;
                    ctx.rect(tanks[i].x - settings.tank.width / 2 , tanks[i].y - 20, 100, 4);
                    ctx.stroke();

                    ctx.beginPath()

                    if(tanks[i].health > 70){
                        ctx.fillStyle = 'green';
                    }
                    else if(tanks[i].health > 40){

                        ctx.fillStyle = 'orange';
                    }
                    else{

                        ctx.fillStyle = 'red ';
                    }

                    ctx.rect(tanks[i].x - 25, tanks[i].y - 19, tanks[i].health, 3);
                    ctx.fill();

               }

            }


        }
    }
};

function clamp(value, min, max){
    if(value < min) return min;
    else if(value > max) return max;
    return value;
}


function getExplosion(){

    var img = document.createElement('img');
    img.setAttribute('src', 'img/explosion/explosion-' + parseInt(explosion.step / 2)+ '.png');


    explosion.step = (explosion.step + 1);

    if(explosion.step > 34){
        explosion.step = -1;
    }

    return img;
}