var moving = false;
var rotating = false;
var shooting = false;

document.addEventListener('keydown', function(e){

    if(!moving){
        // Up
        if(e.keyCode == 38 || e.keyCode == 87){
            socket.send("movingUp");
            moving = true;
        }
        // Down
        else if(e.keyCode == 40 || e.keyCode == 83){
            socket.send("movingDown");
            moving = true;
        }

    }

    if(!rotating){
        // Left
        if(e.keyCode == 37 || e.keyCode == 65){
            socket.send("rotatingLeft");
            rotating = true;
        }
        // Right
        else if(e.keyCode == 39 || e.keyCode == 68){
            socket.send("rotatingRight");
            rotating = true;
        }
    }

    if(e.keyCode == 32 && !shooting){

        socket.send("shoot");
        shooting = true;
    }


})

document.addEventListener('keyup', function(e){

    if(e.keyCode == 38 || e.keyCode == 40 || e.keyCode == 87 || e.keyCode == 83){
        socket.send("stopMoving");
        moving = false;
    }
    else if(e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 65 || e.keyCode == 68){
        socket.send("stopRotating");
        rotating = false;
    }
    else if(e.keyCode == 32){

        shooting = false;
    }

});
